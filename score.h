#ifndef SCORE_H
#define SCORE_H
#include <QGraphicsTextItem>

/*!
 * \brief Die Score-Klasse händelt die Punkte
 *        des Spielers.
 */
class Score:public QGraphicsTextItem{

public:
    Score(QGraphicsTextItem *parent = 0);

    // Erhöht die Punkte des Spielers
    void increase(const int& val);

private:
    int score;      // eigentliche Punktzahl
    int getScore(); // Liefert Punktzahl zurück
};

#endif // SCORE_H
