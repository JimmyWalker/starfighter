#include <QTimer>
#include <QGraphicsScene>
#include <QList>
#include <typeinfo>
#include <QMediaPlayer>

#include "background.h"
#include "game.h"
#include "shot.h"
#include "bomber.h"

// Zugriff auf globale Komponenten
extern Game *game;
extern const int BACKGROUND_SPEED;

// Erzeugt eine Hintergrundgrafik anhand der übergebenen Art
Background::Background(QGraphicsItem *parent, int art):
    QObject(), QGraphicsPixmapItem(parent)
{
    switch (art) {
    case 0:
        setPixmap(QPixmap(":/images/images/bg/background.png"));
        break;
    case 2:
        setPixmap(QPixmap(":/images/images/bg/bg_tal.jpg"));
        break;
    default:
        setPixmap(QPixmap(":/images/images/bg/background.png"));
    }

    // Setzt Bild auf hinterste Anzeigeebene
    this->setZValue(1);
}


void Background::move()
{
    // Position setzen
    setPos(x()-BACKGROUND_SPEED,y());

    // Hintergrund Pattern zusammenfügen wenn Bild den linken Bildschirmran passiert hat
    if(pos().x() + pixmap().width() <= 0)
    {
       setPos(pixmap().width(),0);
    }
 }


