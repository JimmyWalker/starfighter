#include "shot.h"
#include "bomber.h"
#include <QTimer>
#include <QGraphicsScene>
#include <QList>
#include <typeinfo>
#include <game.h>
#include <QMediaPlayer>
#include "heli.h"

// Zugriff auf globales Gameobjekt und statische Werte
extern Game *game;
extern const int MECHANIX_POWER;
extern const int BULLET_DEMAGE;
extern const int SCORE_BY_SHOT;
extern const int SHOT_SPEED;
extern const int EXPLOSION_TIME;

Shot::Shot(QGraphicsItem *parent):
    QObject(), QGraphicsPixmapItem(parent), who(""), explodeLevel(0)
{
    // festlegen der Munitionsgrafik und deren Ebene
    setPixmap(QPixmap(":/images/images/rockets/missile.png"));
    this->setZValue(5);

    // Initialisierung und Connectierung der Timer
    explosionGegner_timer = new QTimer();
    explosionPlayer_timer = new QTimer();
    QObject::connect(explosionGegner_timer, SIGNAL(timeout()),this,SLOT(graphicalExplosionGegner()));
    QObject::connect(explosionPlayer_timer, SIGNAL(timeout()),this,SLOT(graphicalExplosionPlayer()));


    // Konfiguration des Flugsounds des Geschosses
    music = new QMediaPlayer();
    music->setMedia(QUrl("qrc:/sounds/Sounds/missile_launch1.wav"));
    music->play();

    // Container für die Animationsgrafiken der Explosionen am Spieler
    explodeSpritesGegner.append(QPixmap(":/images/images/explosion/explosion1.png"));
    explodeSpritesGegner.append(QPixmap(":/images/images/explosion/explosion2.png"));
    explodeSpritesGegner.append(QPixmap(":/images/images/explosion/explosion3.png"));
    explodeSpritesGegner.append(QPixmap(":/images/images/explosion/explosion4.png"));
    explodeSpritesGegner.append(QPixmap(":/images/images/explosion/explosion5.png"));
    explodeSpritesGegner.append(QPixmap(":/images/images/explosion/explosion6.png"));

    // Container für die Animationsgrafiken der Explosionen des Gegners
    explodeSpritesPlayer.append(QPixmap(":/images/images/explosion/planeExplosion1.png"));
    explodeSpritesPlayer.append(QPixmap(":/images/images/explosion/planeExplosion2.png"));
    explodeSpritesPlayer.append(QPixmap(":/images/images/explosion/planeExplosion3.png"));
    explodeSpritesPlayer.append(QPixmap(":/images/images/explosion/planeExplosion4.png"));
    explodeSpritesPlayer.append(QPixmap(":/images/images/explosion/planeExplosion5.png"));
    explodeSpritesPlayer.append(QPixmap(":/images/images/explosion/planeExplosion6.png"));

    game->list_shots.append(this);
}

Shot::~Shot()
{
    music->stop();
    game->list_shots.removeOne(this);
}

void Shot::move()
{
    // Kollisionen prüfen
    QList<QGraphicsItem* > colliding_items = collidingItems();
    for (int i = 0, n = colliding_items.size(); i < n; i++ )
    {

        // Spieler beschießt Bomber
        if(  // Handelt es sich um den Typ Bomber
                typeid(*(colliding_items[i])) == typeid(Bomber)
             //&& die Kugel wird als Kollisionsgegenstand ausgenommen
                && this->explodeLevel == 0
             // && kommt die Kugel vom Spieler
                && this->getWho() == "Player" ){

            // Explosionssound abspielen
            game->explosion_sound();
            //Punkte erhöhen
            game->score->increase( SCORE_BY_SHOT );
            game->logic->increase_destroyed_enemies();

            // Abgeschossene Objekte löschen und Kugeln
            scene()->removeItem(colliding_items[i]);
            explodeGegner();

            // Entfernen des Gegners und der Kugel
            delete colliding_items[i];
           // delete this;

            // Schleife beenden
            return;
        }

        if(  // Handelt es sich um den Typ Heli
                typeid(*(colliding_items[i])) == typeid(Heli)
            //&& die Kugel wird als Kollisionsgegenstand ausgenommen
               && this->explodeLevel == 0
            // && kommt die Kugel vom Spieler
               && this->getWho() == "Player" ){

           // Explosionssound abspielen
           game->explosion_sound();
           //Punkte erhöhen
           game->score->increase( SCORE_BY_SHOT );
           game->logic->increase_destroyed_enemies();

           // Abgeschossene Objekte löschen und Kugeln
           scene()->removeItem(colliding_items[i]);
           explodeGegner();

           // Entfernen des Gegners und der Kugel
           delete colliding_items[i];
          // delete this;

           // Schleife beenden
           return;
       }


        // Bomber beschießt Spieler
        if(
                typeid(*(colliding_items[i])) == typeid(Spieler)
             //&& die Kugel wird als Kollisionsgegenstand ausgenommen
                && this->explodeLevel == 0
             // && kommt die Kugel vom Gegner
                && this->getWho() == "Gegner" ){

            // Explosionssound abspielen
            game->explosion_sound();
            //Lebenskraft abziehen
            game->health->decrease( BULLET_DEMAGE );

            // Explosion am Player erzeugen
            explodePlayer();

            // Spieler TOD?
            if( game->health->getHealth() <= 0 )
            {
            // Abgeschossene Objekte löschen und Kugeln
            scene()->removeItem(colliding_items[i]);
          //  scene()->removeItem(this);

            delete colliding_items[i];
            delete this;

            // Schleife beenden
            return;
            }
        }
    }

    // SPIELER hat Munition abgefeuert
    // Kugel bzw. Explosion wird bewegt
    if(this->who == "Player")
    {
        if( explodeLevel == 0 )
        {
            setPos(x()+SHOT_SPEED,y());
        }
        else if(explodeLevel > 0)// Explosion behält Position bei
        {
            setPos(x(),y());
        }
    }

    // GEGNER hat Munition abgefeuert
    // Kugel bzw. Explosion wird bewegt
    if(this->who == "Gegner")
    {
        if( explodeLevel == 0 )
        {
            setPos(x()-SHOT_SPEED,y());
        }
        else if(explodeLevel > 0)// Explosion behält Position bei
        {
            setPos(x(),y());
        }
    }

    // Kugel außerhalb des Feldes? (Links und Rechts
    if( pos().x() > (800) || pos().x() < 0 ){
        //Entferne das Geschoss
        scene()->removeItem(this);
        delete this;
    }
}

void Shot::explodePlayer()
{
    explosionPlayer_timer->start(EXPLOSION_TIME);
}

void Shot::explodeGegner()
{
    explosionGegner_timer->start(EXPLOSION_TIME);
}

void Shot::graphicalExplosionGegner()
{
    /***
     *  Für Gegner
     */
    if(explodeLevel == 0)
    {
      // Bildposition der Explosion bestimmen
      setPos(x()-60, y()-50);
    }

    // Aktuelles Sprite laden
    setPixmap(explodeSpritesGegner[explodeLevel]);

    // Erhöung des Spriteindexes
    explodeLevel += 1;

    // Objekt wird nach 6 Animationsbildern gelöscht
    if(explodeLevel >= 5)
    {
        explosionGegner_timer->stop();
        delete explosionGegner_timer;
        scene()->removeItem(this);
        delete this;    }

}


void Shot::graphicalExplosionPlayer()
{
    /***
     *  Für Player
     */
    if(explodeLevel == 0)
    {
      // Bildposition der Explosion bestimmen
      setPos(x(), y()+(pixmap().height()*3));
    }

    // Aktuelles Sprite laden
    setPixmap(explodeSpritesPlayer[explodeLevel]);

    // Erhöung des Spriteindexes
    explodeLevel += 1;

    // Objekt wird nach 6 Animationsbildern gelöscht
    if(explodeLevel >= 5)
    {
        explosionPlayer_timer->stop();
        delete explosionPlayer_timer;
        scene()->removeItem(this);
        delete this;
    }
}

void Shot::setWho(QString who)
{
    this->who = who;
}

QString Shot::getWho()
{
    return who;
}

