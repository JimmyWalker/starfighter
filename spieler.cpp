#include <QGraphicsScene>
#include <QDebug>
#include <QDateTime>
#include <QTimer>

#include "spieler.h"
#include "shot.h"
#include "bomber.h"
#include "game.h"

using namespace std;
extern Game *game;

Spieler::Spieler(QGraphicsItem *parent): QObject(), QGraphicsPixmapItem(parent), turbo_timeStamp(0)
{
    // Spielergrafik wird gesetzt und deiner Ebene zugewiesen
    setPixmap(QPixmap(":/images/images/plane/plane_norm.png"));
    this->setZValue(5);

    // Timer kontrolliert ob der Düsenantrieb ausgeschaltet werden muss
    QTimer *turbo_timer = new QTimer();
    QObject::connect(turbo_timer, SIGNAL(timeout()),this,SLOT(turboOff()));
    turbo_timer->start(50);

}

Spieler::~Spieler()
{
    game->gameOver();
}

void Spieler::keyPressEvent(QKeyEvent *event)
{
    // Nach oben
    if(event->key() == Qt::Key_W)
    {
        // Prüfen auf oberen Rand
        if(pos().y() > 0)
        setPos(x(),y()-10);
    }

    // Nach links
    if(event->key() == Qt::Key_A)
    {
        // Prüfen auf linken Rand
        if(pos().x() > 0)
        setPos(x()-10,y());
    }

    // Nach unten
    if(event->key() == Qt::Key_S)
    {
        // Prüfen auf unteren Rand
        if(pos().y()+pixmap().height()< 600)
        setPos(x(),y()+10);
    }

    // Nach rechts
    if(event->key() == Qt::Key_D)
    {
        // Prüfen auf rechten Rand
        if(pos().x()+pixmap().width() < 800)
        {
            // beim Bewegen nach rechts wird der Düsenantrieb (Grafik aktiviert)
            turbo_timeStamp = QDateTime::currentMSecsSinceEpoch();
            setPixmap(QPixmap(":/images/images/plane/plane_turbo.png"));
            setPos(x()+10,y());
        }
    }

    // Schießen
    if(event->key() == Qt::Key_Space)
    {
        Shot *shot = new Shot();
        // Wer hat die Kugel abgefeuert?
        shot->setWho(QString("Player"));

        // Startkoordinaten des Geschosses
        int start_x = x() + pixmap().width();
        int start_y = y() + ( pixmap().height() / 2 );

        shot->setPos(start_x, start_y);
        scene()->addItem(shot);
    }
}

void Spieler::turboOff()
{
    int currentStamp = QDateTime::currentMSecsSinceEpoch();

    // Feuerantrieb nach x Zeiteinheiten abschalten (nur optisch)
    if( (currentStamp - turbo_timeStamp) > 200)setPixmap(QPixmap(":/images/images/plane/plane_norm.png"));
}

