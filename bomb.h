#ifndef BOMB_H
#define BOMB_H

#include <QString>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>
#include <QMediaPlayer>
#include <QTimer>

/*!
 * \brief Die Bomb-Klasse repräsentiert ein Schussobjekt der Klasse "Bomber"
 */
class Bomb: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT

public:
   Bomb(QGraphicsItem *parent=0);
   // Destruktor löscht die Bombe aus einer zentralen Gameobjektliste
   ~Bomb();

   // Spielt Hintergrundmusik
   QMediaPlayer *music;

private:
   // kontrolliert die Explosionsanimation
   QTimer *explosionGround_timer;

   // Enthält alle Sprites für die Explosion am Boden
   QList<QPixmap> explodeSpritesGround;

   // Spritestufen für Explosionsbilder
   int explodeLevel;

   // Löst den Timer der Explosionsanimation aus
   void explodeGround();


public slots:
   /*!
    * \brief move - ermöglicht die Bewegung innerhalb des Gameloops
    */
   void move();
   /*!
    * \brief graphicalExplosionGround - realisiert die grafische Animation der Explosion
    */
   void graphicalExplosionGround();

};

#endif // BOMB_H
