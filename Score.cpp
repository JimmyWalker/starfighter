#include <QFont>

#include "score.h"

Score::Score(QGraphicsTextItem *parent):
    QGraphicsTextItem(parent), score(0)
{
    // Erzeugt Anzeigegrafik
    setPlainText(QString("Score: ") + QString::number(score) );
    setDefaultTextColor(Qt::blue);
    setFont(QFont("times", 16));
    this->setZValue(2);
}

void Score::increase(const int& val)
{
    score += val;
    setPlainText(QString("Score: ") + QString::number(score) ); // Zeichnet Score neu
}

int Score::getScore()
{
    return score;
}
