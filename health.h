#ifndef HEALTH_H
#define HEALTH_H
#include <QGraphicsTextItem>

/*!
 * \brief Die Health-Klasse händelt die Energieanzeige
 *        des Spielers.
 */
class Health:public QGraphicsTextItem{
public:
    Health(QGraphicsTextItem *parent = 0);

    // Methoden um Lebenspunkte zu erhalten oder zu beeinflussen
    int getHealth();
    void decrease(const int&);
    void increase(const int&);

private:
    int health;
};

#endif // HEALTH_H
