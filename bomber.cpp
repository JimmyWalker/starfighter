#include <QDateTime>
#include <QTimer>
#include <QGraphicsScene>
#include <QList>
#include <stdlib.h>
#include <typeinfo>

#include "bomber.h"
#include "game.h"
#include "logic.h"
#include "bomb.h"
#include "shot.h"

// Zugriff auf globale Komponenten
extern Game *game;
extern const int PLAYER_ENEMY_COLLISION_DEMAGE;
extern const int ENEMY_SPEED;

Bomber::Bomber(QGraphicsItem *parent):
    QObject(), QGraphicsPixmapItem(parent), dodge(false), start_to_dodge(0)
{
    // Zufällige wahl der Position des Gegners
    // 450 Untergrenze damit Flugzeuge nicht am Boden auftauchen
    int random_value = rand() % 450;
    setPos(game->width(), random_value);

    // Animationsbilder
    sprites.append(QPixmap(":/images/images/plane/enemy_01.png"));
    sprites.append(QPixmap(":/images/images/plane/enemy_02.png"));
    sprites.append(QPixmap(":/images/images/plane/enemy_03.png"));

    // Startanimationsbild wird gewählt und die Grafikebene bestimmt
    setPixmap(sprites[0]);
    this->setZValue(5);

    // Objekt wird einer zentralen Liste hinzugefügt
    game->list_bomber.append(this);
}

Bomber::~Bomber()
{
   // Gegnerliste bereinigen
    game->list_bomber.removeOne(this);
}

void Bomber::shoot()
{
    Shot *shot = new Shot();
    // Wer hat geschossen? Spieler oder Gegner?
    shot->setWho("Gegner");
    // Winkel der Kugel anpassen
    shot->setRotation(180);

    // Startkoordinaten des Geschosses
    int start_x = x();
    int start_y = y() + ( pixmap().height() / 2 );
    shot->setPos(start_x, start_y);
    scene()->addItem(shot);
}

void Bomber::move()
{
    // Kollisionen prüfen
    QList<QGraphicsItem* > colliding_items = collidingItems();
    for (int i = 0, n = colliding_items.size(); i < n; i++ )
    {
        // Auf spieler getroffen?
        if(typeid(*(colliding_items[i])) == typeid(Spieler)){

            // Lebenspunkte senken
            game->health->decrease( PLAYER_ENEMY_COLLISION_DEMAGE );

            // Spieler TOD?
            if( game->health->getHealth() <= 0 )
            {
                // Abgeschossene Objekte löschen und Kugeln
                scene()->removeItem(colliding_items[i]);
                scene()->removeItem(this);

                // Spieler würde entfernt werden
                delete colliding_items[i];
                delete this;

                // Schleife beenden
                return;
            }
        }
    }

    // Sprites austauschen
    setPixmap(sprites[0]);
    sprites.append(sprites[0]);
    sprites.removeAt(0);

    // Sprite verschieben
    if(this->dodge) // Ausweichmanöver?
    {
        int current_time = QDateTime::currentMSecsSinceEpoch();
        int move_time = current_time - start_to_dodge;
        int half_display = game->scene->height() / 2;

        // Gegner hat 500 ms um ein Ausweichmanöver zu fliegen
        if(move_time < 500)
        {
            if(this->pos().y() < half_display)  // Nach unten ausweichen
            {
                setPos(x()- (ENEMY_SPEED / 2),y()+ENEMY_SPEED);
            }
            else
            {
                setPos(x()- (ENEMY_SPEED / 2),y()-ENEMY_SPEED);
            }
        }
        else // nach 500 ms wird das Ausweichen abgeschlossen
        {
            this->dodge = false;
        }
    }
    else // normale Bewegung?
    {
        setPos(x()-ENEMY_SPEED,y());
    }

    // Löschen des Gegners nach Verlassen des Bildschirmrandes
    if( pos().x() < 0-pixmap().width() ){
        scene()->removeItem(this);
        delete this;
    }
}


void Bomber::dropBomb()
{
    // Befindet sich der Gegner unter dem Spieler wirft er keine Bomben
    if( pos().y() + pixmap().height() > game->spieler->pos().y())return;

    // Berechnung in welchem Bereich er Bomben abwerfen kann
    if (
            // Maximale Distanz des Gegners zum Spieler bestimmen
            ( pos().x() - (game->spieler->pos().x() + game->spieler->pixmap().width()) ) <= 250
            // Minimale Distanz
         && ( pos().x() - (game->spieler->pos().x() + game->spieler->pixmap().width()) ) > -50
            // Der Gegner muss mindest 100 Pixel über dem Spieler sein
         && ( game->spieler->pos().y() - pos().y() + pixmap().height() ) >=100)
    {
        Bomb *bomb = new Bomb();

        // Startkoordinaten des Geschosses direkt unter dem Gegner
        int start_x = x();
        int start_y = y() + pixmap().height();

        // Werte setzen und der Scene hinzufügen
        bomb->setPos(start_x, start_y);
        scene()->addItem(bomb);
    }
}

void Bomber::move_to_dodge()
{
    // Wenn gerade in der Ausweichephase, dann nicht erneut die Startzeit setzen
    if(dodge)return;

    // setzt Ausweichmodus und Startzeit des Manövers
    this->dodge = true;
    start_to_dodge = QDateTime::currentMSecsSinceEpoch();
}

