#include <QFont>

#include "health.h"
#include "game.h"

extern Game *game;

Health::Health(QGraphicsTextItem *parent):
    QGraphicsTextItem(parent), health(200)
{
    // Konfigurieren der Anzeigewerte und Eigenschaften
    this->setZValue(2);
    setPlainText(QString("Health: ") + QString::number(health) );
    setPos(0,20);
    setDefaultTextColor(Qt::red);
    setFont(QFont("times", 16));
}

void Health::decrease(const int& demage)
{
    // Lebensenergie wird um den Schaden vermindert
    health -= demage;
    if(health < 0)
    {
        health = 0;   // Lebensenergie geht nicht unter 0;
        delete(game->spieler);
    }
    setPlainText(QString("Health: ") + QString::number(health) ); // Redraw Healthpoints
}

void Health::increase(const int& value)
{
    // Lebensenergie wird um entsprechenden Wert erhöht
    health += value;
    setPlainText(QString("Health: ") + QString::number(health) ); // Redraw Healthpoints
}

int Health::getHealth()
{
    return health;
}
