#ifndef HELI_H
#define HELI_H

#include <QGraphicsPixmapItem>
#include <QGraphicsItem>
#include <QObject>
#include <QList>

/*!
 * \brief Die Heli-Klasse repräsentiert eine Form von
 *        Gegnerischen Einheiten "Helikopter". Diese sind mit
 *        Raketen bewaffnet.
 */
class Heli: public QObject, public QGraphicsPixmapItem
{
    // Signals and Slots möglich machen
    Q_OBJECT

public:
   Heli(QGraphicsItem * parent=0);
   ~Heli();

   void shoot(); // der Gegner feuert eine normale Rakete ab
   void move(); // steuert die Bewegung

private:
   // Enthält die Spriteanimationen
   QList<QPixmap> sprites;
};

#endif // HELI
