#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>
#include <QMediaPlayer>

/*!
 * \brief Die Background-Klasse repräsentiert die Hintergrundgrafik
 *        die in das Spiel eingefügt wird. Erbt von der Klasse
 *        QGraphicsPixmapItem. Dies ermöglicht die grafische Darstelltung,
 *        sowie deren Manipulation
 */
class Background: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT

public:
    /*!
    * \brief Background
    * \param art - integer der die Art des Hintergrundes symbolisiert
    *              0 = grüner Wald, 1 = Tal-Landschaft
    */
   Background(QGraphicsItem *parent=0, int art = 0);

public slots:
   /*!
    * \brief move - ermöglicht die Bewegung des Bildes während des Gameloopdurchlaufs
    */
   void move();
};

#endif // BACKGROUND_H
