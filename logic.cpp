#include <QGraphicsScene>
#include <QDebug>
#include <QDateTime>

#include "logic.h"
#include "bomber.h"
#include "clouds.h"
#include "game.h"
#include "item.h"

// Zugriff auf globales Gameobjekt und statischer Werte
extern Game *game;
extern const int SCREEN_CLEAR_TIME;
extern const int NUMBER_TO_CLEAR_STAGE;

Logic::Logic():
    QObject(), QGraphicsPixmapItem(parent)
{
    // Abgeschossene Gegner werden zwecks Strategieauswahl gezählt
    destroyed_enemies = 0;
    // Gegnerwelle
    enemy_wave = 1;

    // Werte für den Levelwechsel vorbereiten
    isStageClear = false;
    startClearing = 0;
}


void Logic::spawnEnemy()
{
    // Je Levelabschnitt werden andere Gegner erzeugt
    // (derzeit nur 2 unterschiedliche Abschnitte, danach wiederholt es sich)
    switch (enemy_wave) {
    case 1:
        create_bomber();
        break;

    case 2:
        create_helicopter();
        break;

    case 3:
        create_bomber();
        break;

    default:
        create_bomber();
        break;
    }

}

void Logic::spawnCloud()
{
    Clouds *cloud = new Clouds();
    scene()->addItem(cloud);
}

void Logic::spawnItem()
{
    Item *item = new Item();
    scene()->addItem(item);
}

void Logic::chooseLogic()
{

    // Wenn kein Gegner in der Liste ist, wird keine Strategie gewählt
    if(game->list_bomber.size() == 0 && enemy_wave == 1)return;
    if(game->list_heli.size() == 0 && enemy_wave == 2)return;

    // Wähle Gegner für den eine Strategie zugewiesen werden soll
    int id_enemy = 0;
    if(enemy_wave == 1)id_enemy = ( rand() % game->list_bomber.size());
    if(enemy_wave == 2)id_enemy = ( rand() % game->list_heli.size());

    // Wähle eine von 3 Strategien
    int strategy = (rand() % 3);

    switch (strategy) {
    // Bombe Fallen lassen
    case 0:
        if(enemy_wave == 1)game->list_bomber[id_enemy]->dropBomb();
        break;

    // Schiessen wenn Player auf gleicher Höhe ist
    case 1:
        if(enemy_wave == 1)game->list_bomber[id_enemy]->shoot();
        if(enemy_wave == 2)game->list_heli[id_enemy]->shoot();
        break;

    // Gegner weichen von ihrer aktuellen Position ab
    case 2:
        if(enemy_wave == 1)game->list_bomber[id_enemy]->move_to_dodge();
        break;

    default:
        qDebug()<< "[FEHLER] keine der Strategien gewählt";
    }
}

void Logic::increase_destroyed_enemies()
{
    this->destroyed_enemies++;
}

void Logic::reset_destroyed_enemies()
{
    this->destroyed_enemies = 0;
}

void Logic::increase_enemy_wave()
{
    this->enemy_wave++;
}

void Logic::check_stage_cleared()
{
    int currentTime = QDateTime::currentMSecsSinceEpoch();

    // Prüfen ob genug Gegner für den Levelabschnitt vernichtet wurden
    if(destroyed_enemies >= NUMBER_TO_CLEAR_STAGE)
    {
        // Levelabschnitt erreicht, neues Level einleiten
        isStageClear = true;
        startClearing = QDateTime::currentMSecsSinceEpoch();
        increase_enemy_wave();
        destroyed_enemies = 0;
        game->showStageClear();
    }

    /*  Befindet sich das Level in der Übergangsphase wird ein
     *  Blackscreen erzeugt.
    */
    if(isStageClear && (currentTime - startClearing) <= SCREEN_CLEAR_TIME)
    {
            game->setOpacityStageClear();
    }

    /*
     * Ist die Levelübergangsphase abgeschlossen,
     * wird der Übergangsscreen und der StageCleared Schriftzug entfernt,
     * ebenso der Hintergrund gewechselt
     */
    if(isStageClear && (currentTime - startClearing) >= SCREEN_CLEAR_TIME )
    {
        change_background();
        isStageClear = false;
        game->logic->startClearing = 0;
        game->blackScreenOpacityVal = 0;
        change_background();
        game->removeStageClear();
    }
}

void Logic::create_bomber()
{    
    Bomber *bomber = new Bomber();
    scene()->addItem(bomber);
}

void Logic::create_helicopter()
{
    Heli *heli = new Heli();
    scene()->addItem(heli);
}



void Logic::change_background()
{
    game->bg->setPixmap(QPixmap(":/images/images/bg/bg_tal.jpg"));
    game->bg2->setPixmap(QPixmap(":/images/images/bg/bg_tal.jpg"));
}

