#ifndef SHOT_H
#define SHOT_H

#include <QString>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>
#include <QMediaPlayer>

/*!
 * \brief Die Shot-Klasse repräsentiert ein Munitonsobjekt
 *        in Form einer Rakte. Diese kann sowohl vom Spieler
 *        als auch vom Gegner genutzt werden.
 */
class Shot: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT

public:
   Shot(QGraphicsItem *parent=0);
   ~Shot();

   // Sound das fliegen der Rakete
   QMediaPlayer * music;
   // Timer der die Explosionen (grafisch) steuert
   QTimer *explosionGegner_timer;
   QTimer *explosionPlayer_timer;

   // Container mit den Spieler und Gegnergrafiken
   QList<QPixmap> explodeSpritesGegner;
   QList<QPixmap> explodeSpritesPlayer;

   // Wer hat die Kugel abgeschossen? (Kugeln müssen später noch identifizierbar sein)
   QString who;

   // Wie weit ist die Explosion fortgeschritten?
   int explodeLevel;

   // Aktiviert die Timer für die Explosion
   void explodeGegner();
   void explodePlayer();

   // ermöglicht es Festzulegen von wem die Kugel kommt
   void setWho(QString who);

   // Bewegungsroutine des Geschosses
   void move();

   // Gibt den Absender des Geschosses preis
   inline QString getWho();

public slots:
   // Umsetzen der grafischen Animation, gesteuert durch die Timer (s.o.)
   void graphicalExplosionGegner();
   void graphicalExplosionPlayer();

};

#endif // SHOT_H
