#ifndef BOMBER_H
#define BOMBER_H

#include <QGraphicsPixmapItem>
#include <QGraphicsItem>
#include <QObject>
#include <QList>

/*!
 * \brief Die Bomber-Klasse repräsentiert eine Form von
 *        Gegnerischen Einheiten "Bomber". Diese sind mit
 *        Raketen und Bomben bewaffnet und ebenso in der Lage
 *        Ausweichmanöver zu fliegen.
 */
class Bomber: public QObject, public QGraphicsPixmapItem
{
    // Signals and Slots möglich machen
    Q_OBJECT

public:
   Bomber(QGraphicsItem * parent=0);
   // Destruktor löscht das Objekt aus einer zentralen Gameobjektliste
   ~Bomber();

   // Ermöglicht das Bewegen innerhalb des Gameloops
   void move();

   // alternative Ausweichbewegung des Gegners
   void move_to_dodge();

   // der Gegner feuert eine normale Rakete ab
   void shoot();

private:
   // Enthält die Spriteanimationen
   QList<QPixmap> sprites;

   // Erfasst die Startzeit des Ausweichens
   int start_to_dodge;
   // befindet sich der Gegner im Ausweichmodus?
   bool dodge;


public slots:
   // Erlaubt der Logik-Klasse Bomben beim Objekt zu aktivieren
   void dropBomb();

};

#endif // BOMBER
