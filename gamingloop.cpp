#include <QDebug>
#include <QDateTime>

#include "gamingloop.h"
#include "game.h"

// Zugriff auf globales Gameobjekt
extern Game *game;

Gamingloop::Gamingloop():
    current_time(0), elapsed_time(0)
{
    // Initialisierung erster "vorhergender" Zeitpunkt
    previous_time = QDateTime::currentMSecsSinceEpoch();
}


void Gamingloop::update()
{
    // Zeitwerte werden bei jeder Runde aktualisiert
    current_time = QDateTime::currentMSecsSinceEpoch();
    elapsed_time = current_time - previous_time;
    previous_time = current_time;

    // Elemente werden bewegt
    move_enemies();
    move_items();
    move_clouds();
    move_background();

    // Kugeln werden in kürzeren Zeitabständen verrückt
    if(elapsed_time >= 30)move_ammo();
    // Prüfung ob Spieler die Gegnerwelle bezwungen hat
    game->logic->check_stage_cleared();
}

/***
 *  Folgende Methoden Arbeiten analog:
 *  Befindet sich ein Objekt in der Liste, so wird es bewegt!
 */
void Gamingloop::move_enemies()
{
    if(game->list_bomber.count() > 0)
    {
        for(int i = 0; i < game->list_bomber.count(); i++) game->list_bomber[i]->move();
    }

    if(game->list_heli.count() > 0)
    {
        for(int i = 0; i < game->list_heli.count(); i++) game->list_heli[i]->move();
    }
}

void Gamingloop::move_items()
{
    for(int i = 0; i < game->list_items.count(); i++) game->list_items[i]->move();

}

void Gamingloop::move_clouds()
{
    for(int i = 0; i < game->list_clouds.count(); i++) game->list_clouds[i]->move();
}

void Gamingloop::move_background()
{
    for(int i = 0; i < game->list_backgrounds.count(); i++) game->list_backgrounds[i]->move();
}

void Gamingloop::move_ammo()
{
    for(int i = 0; i < game->list_shots.count(); i++) game->list_shots[i]->move();

    for(int i = 0; i < game->list_bombs.count(); i++) game->list_bombs[i]->move();

}
