#include "heli.h"
#include <QTimer>
#include <QGraphicsScene>
#include <QList>
#include <stdlib.h>
#include "game.h"
#include "logic.h"
#include <typeinfo>
#include "bomb.h"
#include "shot.h"
#include "QDateTime"

// Zugriff auf globale Gameobjekte und statische Werte
extern Game *game;
extern const int PLAYER_ENEMY_COLLISION_DEMAGE;
extern const int ENEMY_SPEED;

Heli::Heli(QGraphicsItem *parent):
    QObject(), QGraphicsPixmapItem(parent)
{
    // 450 Untergrenze damit Flugzeuge nicht am Boden auftauchen
    int random_value = rand() % 450;
    setPos(game->width(), random_value);

    // Füllt Liste mit Animationsbildern
    sprites.append(QPixmap(":/images/images/plane/enemy/heli/heli_01.png"));
    sprites.append(QPixmap(":/images/images/plane/enemy/heli/heli_02.png"));
    sprites.append(QPixmap(":/images/images/plane/enemy/heli/heli_03.png"));
    sprites.append(QPixmap(":/images/images/plane/enemy/heli/heli_04.png"));
    sprites.append(QPixmap(":/images/images/plane/enemy/heli/heli_05.png"));
    sprites.append(QPixmap(":/images/images/plane/enemy/heli/heli_06.png"));
    sprites.append(QPixmap(":/images/images/plane/enemy/heli/heli_07.png"));
    sprites.append(QPixmap(":/images/images/plane/enemy/heli/heli_08.png"));

    // Setzt Startbild und legt Grafikebene fest
    setPixmap(sprites[0]);
    this->setZValue(5);

    game->list_heli.append(this);
}

Heli::~Heli()
{
   // Gegnerliste bereinigen
    game->list_heli.removeOne(this);
}

void Heli::shoot()
{
    Shot *shot = new Shot();
    // Wer hat geschossen?
    shot->setWho("Gegner");
    // Winkel des Flugkörpers anpassen
    shot->setRotation(180);

    // Startkoordinaten des Geschosses
    int start_x = x();
    int start_y = y() + ( pixmap().height() / 2 );

    shot->setPos(start_x, start_y);
    scene()->addItem(shot);
}

void Heli::move()
{
    // Kollisionen prüfen
    QList<QGraphicsItem* > colliding_items = collidingItems();
    for (int i = 0, n = colliding_items.size(); i < n; i++ )
    {
        // Prüfen ob der Gegner den Spieler getroffen hat
        if(typeid(*(colliding_items[i])) == typeid(Spieler)){

            // Lebenspunkte senken
            game->health->decrease( PLAYER_ENEMY_COLLISION_DEMAGE );

            // Spieler TOD?
            if( game->health->getHealth() <= 0 )
            {
            // Abgeschossene Objekte und Kugeln löschen
            scene()->removeItem(colliding_items[i]);
            scene()->removeItem(this);

            delete colliding_items[i];
            delete this;

            // Schleife beenden
            return;
            }
        }
    }

    // Sprites austauschen
    setPixmap(sprites[0]);
    sprites.append(sprites[0]);
    sprites.removeAt(0);

    // Sprite verschieben
    setPos(x()-ENEMY_SPEED,y());

    // Löschen der Gegner nach Verlassen des Bildschirmrandes
    if( pos().x() < 0-pixmap().width() ){
        scene()->removeItem(this);
        delete this;
    }
}

