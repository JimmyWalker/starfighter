#include <QApplication>
#include "game.h"

// Objekt des Spielfeldes
Game *game;

/*!
 * \brief main - Startpunkt der gesamten Applikation
 *
 * Fakten zum Spiel:
 * Es handelt sich um ein Typisches 2D Flugzeug-Action Spiel aus vergangen Zeiten.
 * Der Sound und die Grafiken wirken stilistisch sehr retrohaft.
 *
 * Der Spieler selbst hat einen Düsenjet, der er durch die einzelnen Levelabschnitte
 * manövrieren muss. Er ist bewaffnet mit Raketen, die er für die Bekämpung seiner
 * Gegner (weitere Flugobjekte) einsetzen muss.
 *
 * Das Spiel ist in dieser Implementationsphase noch ein Demoprojekt und verfügt über
 * 2 unterschiedliche Level. Ab Level 3 wiederholt sich der Ablauf.
 *
 */
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // Erstellt das Spielfeld und zeigt es
    game = new Game();
    game->show();

    return a.exec();
}





