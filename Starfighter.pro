#-------------------------------------------------
#
# Project created by QtCreator 2015-06-10T12:07:41
#
#-------------------------------------------------

QT       += core gui \
         multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = StarFighter
TEMPLATE = app


SOURCES += main.cpp\
    gamedata.cpp \
    shot.cpp \
    game.cpp \
    health.cpp \
    background.cpp \
    spieler.cpp \
    logic.cpp \
    bomb.cpp \
    clouds.cpp \
    item.cpp \
    gamingloop.cpp \
    bomber.cpp \
    heli.cpp \
    score.cpp

HEADERS  += \
    shot.h \
    game.h \
    health.h \
    background.h \
    spieler.h \
    logic.h \
    bomb.h \
    clouds.h \
    item.h \
    gamingloop.h \
    bomber.h \
    heli.h \
    score.h

FORMS    += mainwindow.ui \
    form.ui

RESOURCES += \
    ress.qrc
