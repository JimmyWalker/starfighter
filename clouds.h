#ifndef CLOUDS_H
#define CLOUDS_H

#include <QGraphicsPixmapItem>
#include <QGraphicsItem>
#include <QObject>
#include <QDebug>
#include <QList>

/*!
 * \brief Die Klasse Clouds - repräsentiert ein Wolkenobjekt
 */
class Clouds: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
   Clouds(QGraphicsItem * parent=0);
   ~Clouds();

private:
    QList<QPixmap> cloudList;
    int wolkenArt;

public slots:
   // Bewegt das Wolkengebilde
   void move();

};
#endif // CLOUDS_H
