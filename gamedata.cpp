/*!
 *  Enthält alle wichtigen Werte die für das Spiel wichtig sind
 */

/*
 *  ######## Geschwindigkeitskonstanten ########
 */

extern const int BACKGROUND_SPEED = 1;

extern const int CLOUD_1_SPEED = 7;
extern const int CLOUD_2_SPEED = 2;
extern const int CLOUD_3_SPEED = 5;
extern const int CLOUD_4_SPEED = 1;

extern const int ENEMY_SPEED = 3;

extern const int ITEM_SPEED = 1;

extern const int BOMB_FALL_SPEED_X = 5;
extern const int BOMB_FALL_SPEED_Y = 6;

extern const int SHOT_SPEED = 8;

/*
 *  ######## Animationsübergangszeiten ########
 */

extern const int EXPLOSION_TIME = 20;
extern const int SCREEN_CLEAR_TIME = 6000;


/*
 *  ######## Schaden / Heilung und Punkte / Clear Stagevoraussetzung ########
 */

extern const int BOMB_DEMAGE   = 40;
extern const int BULLET_DEMAGE = 5;
extern const int PLAYER_ENEMY_COLLISION_DEMAGE = 1;
extern const int MECHANIX_POWER = 75;
extern const int SCORE_BY_SHOT = 25;

// Es müssen je Level 15 Gegner abgeschossen werden
extern const int NUMBER_TO_CLEAR_STAGE = 15;


/*
 *  ######## Grafikebenen ########
 */

// Ebene aller kollidierbarer Elemente (Gegner/Spieler, Schüsse)
extern const int Z_COLLISION = 5;
// Ebene Größe 1
extern const int Z_CLOUD_1 = 10;
// Ebene Größe 2
extern const int Z_CLOUD_2 = 4;
// Ebene Größe 3
extern const int Z_CLOUD_3 = 3;
// Ebene Größe 4
extern const int Z_CLOUD_4 = 2;

