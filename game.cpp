#include <QApplication>
#include <QDebug>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsRectItem>
#include <QMediaPlayer>
#include <QGraphicsItem>
#include <QList>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <typeinfo>
#include <QProcess>

#include "game.h"
#include "spieler.h"
#include "logic.h"
#include "item.h"
#include "background.h"
#include "bomber.h"


Game::Game()
{
    // Größe des Fensters bestimmen und Scene setzen
    scene = new QGraphicsScene();
    scene->setSceneRect(0,0,800,600);
    setScene(scene);

    // Blackscreen für Levelübergänge
    blackScreen = new QGraphicsRectItem(0,0,800,600);

    // weitere Szeneneinstellungen
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(800,600);

    init();

    show();
}

void Game::init()
{
    // Baclkscreen vorkonfigurieren
    blackScreenOpacityVal = 0;
    blackScreen->setZValue(29);
    blackScreen->setBrush(QBrush(Qt::black));
    blackScreen->setOpacity(0);
    scene->addItem(blackScreen);

    // Schriftzug "StageCleared" konfigurieren
    stageClearScreen = new QGraphicsPixmapItem();
    stageClearScreen->setPixmap(QPixmap(":/images/images/StageClear.png"));
    stageClearScreen->setZValue(30);
    stageClearScreen->setPos( ( width()-stageClearScreen->pixmap().width() ) / 2, ( height() / 2) - stageClearScreen->pixmap().height() );
    stageClearScreen->setVisible(false);
    scene->addItem(stageClearScreen);

    // Spieler erzeugen
    spieler = new Spieler();

    // Hintergründe erzeugen
    bg = new Background(0,0);
    bg2 = new Background(0,0);
    scene->addItem(bg);
    scene->addItem(bg2);
    bg2->setPos(2039,0);

    this->list_backgrounds.append(bg);
    this->list_backgrounds.append(bg2);

    spieler->setFlag(QGraphicsItem::ItemIsFocusable);
    spieler->focusItem();
    scene->addItem(spieler);
    // Focus auf Spieler setzen, damit er steuerbar ist
    scene->setFocusItem(spieler);

    // Positioning the player
    spieler->setPos(100,(scene->height()/2)-spieler->pixmap().height());

    // Create the Score
    score = new Score();
    scene->addItem(score);

    health = new Health();
    scene->addItem(health);


    // Play BG Music
    QMediaPlayer * music = new QMediaPlayer();
    music->setMedia(QUrl("qrc:/sounds/Sounds/bg_sound.mp3"));
    music->play();


    // Create Logic
    logic = new Logic();
    scene->addItem(logic);

    // Create Gameloop
    gameloop = new Gamingloop();

    // Spawn Enemies
    spawn_timer = new QTimer();
    QObject::connect(spawn_timer, SIGNAL(timeout()),logic,SLOT(spawnEnemy()));
    spawn_timer->start(2000);

    // Spawn Clouds
    cloud_timer = new QTimer();
    QObject::connect(cloud_timer, SIGNAL(timeout()),logic,SLOT(spawnCloud()));
    cloud_timer->start(9000);

    // Spawn items
    item_timer = new QTimer();
    QObject::connect(item_timer, SIGNAL(timeout()),logic,SLOT(spawnItem()));
    item_timer->start(30500);


    // Ausführen der Logik in Zeitabständen X
    logic_timer = new QTimer();
    QObject::connect(logic_timer, SIGNAL(timeout()),logic,SLOT(chooseLogic()));
    logic_timer->start(1000);

    loop_timer = new QTimer();
    connect(loop_timer, SIGNAL(timeout()),gameloop,SLOT(update()));
    loop_timer->start(20);


}



void Game::explosion_sound()
{
    QMediaPlayer *explosion = new QMediaPlayer();
    explosion->setMedia(QUrl("qrc:/sounds/Sounds/missile_explosion.wav"));
    explosion->play();
}

void Game::gameOver()
{
    // Alle Timer die Prozesse steuern entfernen
        delete_Timer();

    // Game Over anzeigen
        QGraphicsPixmapItem *gameOver = new QGraphicsPixmapItem();
        gameOver->setPixmap(QPixmap(":/images/images/gameOver.png"));
        gameOver->setZValue(30);
    // Position bestimmen und zuweisen
        int posX = ( this->width() - gameOver->pixmap().width() ) / 2;
        int posY = ( this->height() - gameOver->pixmap().height() - 300) / 2;

        gameOver->setPos(posX, posY);
        scene->addItem(gameOver);


    // Exit Game Button
        exitGame = new QGraphicsPixmapItem();
        exitGame->setPixmap(QPixmap(":/images/images/exitgame.png"));
        exitGame->setZValue(30);
        posX = ( gameOver->pos().x() + gameOver->pixmap().width() / 2) + 30;
        posY = ( gameOver->pos().y() + gameOver->pixmap().height() + 30);
        exitGame->setPos(posX, posY);
        scene->addItem(exitGame);


    // New Game Button
        newGame = new QGraphicsPixmapItem();
        newGame->setPixmap(QPixmap(":/images/images/newgame.png"));
        newGame->setZValue(30);
        posX = ( gameOver->pos().x() + ( gameOver->pixmap().width() / 2) - 30 - exitGame->pixmap().width());
        posY = exitGame->y();
        newGame->setPos(posX, posY);
        scene->addItem(newGame);


    // Hintergrundmusik abspielen
        QMediaPlayer * game_oversound = new QMediaPlayer();
        game_oversound->setMedia(QUrl("qrc:/sounds/Sounds/gameover.wav"));
        game_oversound->play();

}

void Game::showStageClear()
{
   stageClearScreen->setVisible(true);
}

void Game::removeStageClear()
{
   blackScreen->setOpacity(0);
   stageClearScreen->setVisible(false);
}

// Beim Aufruf wird der Transparenzwert des Blackscreens herabgesetzt
void Game::setOpacityStageClear()
{
    blackScreen->setOpacity(blackScreenOpacityVal);
    blackScreenOpacityVal += 0.01;
}


void Game::mousePressEvent(QMouseEvent *mevent)
{
    // Drücken des Exitbuttons
    if(exitGame == itemAt(mevent->pos())) qApp->exit();

    // Neues Spiel läd das Programm neu
    if(newGame == itemAt(mevent->pos()))
    {
        qApp->quit();
        QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
    }
}

void Game::delete_Timer()
{
    logic_timer->stop();
    logic_timer->disconnect();
    logic_timer->deleteLater();

    item_timer->stop();
    item_timer->disconnect();
    item_timer->deleteLater();

    spawn_timer->stop();
    spawn_timer->disconnect();
    spawn_timer->deleteLater();

    cloud_timer->stop();
    cloud_timer->disconnect();
    cloud_timer->deleteLater();
}




