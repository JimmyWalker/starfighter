#ifndef ITEM_H
#define ITEM_H

#include <QGraphicsPixmapItem>
#include <QGraphicsItem>
#include <QObject>
#include <QDebug>
#include <QList>

/*!
 * \brief Die Item-Klasse wird benötigt um dem Spieler
 *        mit Fähigkeiten zu versorgen (in dieser Implementationsphase
 *        erhält der Spieler lediglich Lebensenergie zurück.
 */
class Item: public QObject, public QGraphicsPixmapItem
{
    // Signals and Slots möglich machen
    Q_OBJECT

public:
   Item(QGraphicsItem * parent=0);
   ~Item();

    // Container der alle Bilder für sammelbare Items bereitstellt
    // Im Moment nur 1 Item implementiert
    QList<QPixmap> itemList;

private:
    int itemArt;                // Um welches Item handelt es sich?
    int rotationAngle;          // Item rotiert, dieser Wert speichert den aktuellen Winkel
    inline void changeAngle();  // Ändert den Winkel

public slots:
   void move();

};
#endif // ITEM_H
