#include <QGraphicsScene>
#include <QList>
#include <typeinfo>
#include <QMediaPlayer>

#include "bomb.h"
#include "bomber.h"
#include "game.h"

// Zugriff auf globale Komponenten
extern Game *game;
extern const int Z_COLLISION;
extern const int BOMB_FALL_SPEED_X;
extern const int BOMB_FALL_SPEED_Y;
extern const int BOMB_DEMAGE;

Bomb::Bomb(QGraphicsItem *parent):
    QObject(), QGraphicsPixmapItem(parent), explodeLevel(0)
{
    // Grafik wird gesetzt und die Ebene zugewiesen
    setPixmap(QPixmap(":/images/images/rockets/bomb.png"));
    this->setZValue( Z_COLLISION );

    // Timer zur koordinierung der Explosionsanimation
    explosionGround_timer = new QTimer();
    QObject::connect(explosionGround_timer, SIGNAL(timeout()),this,SLOT(graphicalExplosionGround()));

    // Animationsbilder für Exploson laden
    explodeSpritesGround.append(QPixmap(":/images/images/explosion/groundExplosion1.png"));
    explodeSpritesGround.append(QPixmap(":/images/images/explosion/groundExplosion2.png"));
    explodeSpritesGround.append(QPixmap(":/images/images/explosion/groundExplosion3.png"));
    explodeSpritesGround.append(QPixmap(":/images/images/explosion/groundExplosion4.png"));
    explodeSpritesGround.append(QPixmap(":/images/images/explosion/groundExplosion5.png"));
    explodeSpritesGround.append(QPixmap(":/images/images/explosion/groundExplosion6.png"));

    // Erzeugte Bombe wird zentral in einer Liste abgelegt
    game->list_bombs.append(this);
}

Bomb::~Bomb()
{
   game->list_bombs.removeOne(this);
}

void Bomb::move()
{
    // Bombe in Bewegung?
    if( this->explodeLevel == 0)
    {
        setPos( x() - BOMB_FALL_SPEED_X, y() + BOMB_FALL_SPEED_Y);
    }
    else // Explosionen bewegen sich nicht
    {
        setPos(x(),y());
    }

    // Kollision mit Spieler oder Boden?
    QList<QGraphicsItem* > colliding_items = collidingItems();
    for (int i = 0, n = colliding_items.size(); i < n; i++ )
    {
        // Kollision der Bombe wenn noch nicht in Explosionsphase
        if(typeid(*(colliding_items[i])) == typeid(Spieler)
                && this->explodeLevel == 0){

            // Explosionssound abspielen
            game->explosion_sound();

            // Lebenspunkte senken
            game->health->decrease( BOMB_DEMAGE );

            // Bombe nach Aufprall entfernen
            scene()->removeItem(this);
            delete this;

            // Keine Kollsionen mehr möglich
            return;
        }
    }    

   // Bombe auf Boden aufgeschlagen?
   if( pos().y() > ( game->height() - pixmap().height() ) && explodeLevel == 0 ){
       explodeLevel = 1;
       explodeGround();
    }
}

void Bomb::explodeGround()
{
   explosionGround_timer->start(150);
}

void Bomb::graphicalExplosionGround()
{
    /***
     * Grafische Animation Für Bodenexplosion
     */
    if(explodeLevel == 1)
    {
      // Sound für Explosion abspielen
      game->explosion_sound();

      // Bildposition der Explosion bestimmen
      setPos( x()- (explodeSpritesGround[explodeLevel].rect().width() / 2), y()+40-explodeSpritesGround[explodeLevel].rect().height());
    }

    // Aktuelles Sprite laden
    setPixmap(explodeSpritesGround[explodeLevel]);

    // Erhöung des Spriteindexes
    explodeLevel += 1;

    // Objekt wird nach 6 Animationsbildern gelöscht
    if(explodeLevel >= 5)
    {
        // Timer und Bombe werden gelöscht bzw. aus der Scene entfernt
        explosionGround_timer->stop();
        delete explosionGround_timer;
        scene()->removeItem(this);
        delete this;
    }
}
