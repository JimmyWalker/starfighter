#ifndef GAMINGLOOP
#define GAMINGLOOP
#include <QObject>

/*!
 * \brief Die Gamingloop-Klasse realisiert den Kern der Bewegungsabläufe des Spiels.
 *        Anhand von Zeitmessungen werden Gegner, Items, Wolken, Hintergrundgrafik und Munition bewegt.
 */
class Gamingloop: public QObject
{
    Q_OBJECT
public:
   Gamingloop();

private:
   int previous_time;   // vorhergehende Zeit
   int current_time;    // aktuelle zeit
   int elapsed_time;    // Vergangene zeit

   // Methoden die im während der Updatefunktion aufgerufen werden
   inline void move_enemies();
   inline void move_items();
   inline void move_clouds();
   inline void move_background();
   inline void move_ammo();

public slots:
   /*!
    * Kerfunktion der Klasse,
    * der Slot repräsentiert den Gameloop.
    */
    void update();
};

#endif // GAMINGLOOP

