#include <QGraphicsScene>
#include <QList>
#include <typeinfo>

#include "clouds.h"
#include "game.h"
#include "logic.h"
#include "bomb.h"
#include "shot.h"


extern Game *game;

// Grafische Wolkenebenen
extern const int Z_CLOUD_1;
extern const int Z_CLOUD_2;
extern const int Z_CLOUD_3;
extern const int Z_CLOUD_4;

// Geschwindigkeit der Wolken
extern const signed int CLOUD_1_SPEED;
extern const signed int CLOUD_2_SPEED;
extern const signed int CLOUD_3_SPEED;
extern const signed int CLOUD_4_SPEED;

Clouds::Clouds(QGraphicsItem *parent):
    QObject(), QGraphicsPixmapItem(parent)
{

    // Randomposition abzgl. 300 damit am Boden keine Wolken auftauchen
    int randomPosition = rand() % (game->height() - 300);

    setPos(game->width(), randomPosition);

    // Laden der Wolkenbilder
    cloudList.append(QPixmap(":/images/images/clouds/cloud1.png")); // 150x122
    cloudList.append(QPixmap(":/images/images/clouds/cloud2.png")); // 300x159
    cloudList.append(QPixmap(":/images/images/clouds/cloud3.png")); // 160x139
    cloudList.append(QPixmap(":/images/images/clouds/cloud4.png")); // 520x179

    // Random Wolkenart wählen und als Bild setzen
    wolkenArt = rand() % (cloudList.count());
    setPixmap(cloudList[wolkenArt]);

    game->list_clouds.append(this);

    // Wolkenarten auf grafischer Ebene anordnen
    // Kleinere nach vorn, Größere nach Hinten (Parallaxeffekt)
    switch ( wolkenArt ) {
    case 0:
    this->setZValue(Z_CLOUD_1);
        break;

    case 1:
        this->setZValue(Z_CLOUD_2);
        break;

    case 2:
        this->setZValue(Z_CLOUD_3);
        break;

    case 3:
        this->setZValue(Z_CLOUD_4);
        break;

    default:
        break;
    }
}

Clouds::~Clouds()
{   // Wolke aus zentraler Liste entfernen
    game->list_clouds.removeOne(this);
}


void Clouds::move()
{
    // Wolken werden je nach größe unterschiedlich schnell bewegt
    if( wolkenArt == 0)setPos( x() - CLOUD_1_SPEED,y() );
    if( wolkenArt == 1)setPos( x() - CLOUD_2_SPEED,y() );
    if( wolkenArt == 2)setPos( x() - CLOUD_3_SPEED,y() );
    if( wolkenArt == 3)setPos( x() - CLOUD_4_SPEED,y() );

    // Löschen der Wolke nach Verlassen des Bildschirmrandes
    if( pos().x() < 0-pixmap().width() ){
        scene()->removeItem(this);
        delete this;
    }
}


