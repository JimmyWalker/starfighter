#ifndef LOGIC_H
#define LOGIC_H

#include <QGraphicsItem>
#include <QObject>

#include "bomber.h"
#include "clouds.h"

/*!
 * \brief Die Logik-Klasse steuert das Spiel.
 *        Sie legt u.a. fest welche Strategie ein Gegner nutzt.
 */
class Logic: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT

public:
   Logic(QGraphicsItem * parent=0);

   void increase_destroyed_enemies(); // Zählt die zerstörten Gegner hoch
   void check_stage_cleared(); // Prüft ob das Levelende erreicht wurde (anhand der zerstörten Gegner)

private:
   int destroyed_enemies;  // Enthält die Anzahl vernichteter Gegner je Levelabschnitt
   int enemy_wave;         // In welchem Level befindet sich der Gegner
   int startClearing;      // Zeitpunkt an dem das Level erledigt wurde
   bool isStageClear;      // Level geschafft?

   void reset_destroyed_enemies(); // Setzt die Zahl der zerstörten Gegner zurück
   void increase_enemy_wave();     // Setzt das Level um 1 nach oben
   void change_background();       // Ändert die Hintergrundgrafik
   void create_bomber();           // erzeugt Gegner "Bomber"
   void create_helicopter();       // erzeugt Gegner "Helikopter"

public slots:
    void spawnEnemy();             // Erzeugt je nach Level verschiedene Gegner
    void spawnCloud();             // Setzt Wolken in das Spiel
    void spawnItem();              // Erzeugt Items (z.B. Lebensenergie)
    void chooseLogic();            // Wählt die Logik für die Gegner
};

#endif // LOGIC_H
