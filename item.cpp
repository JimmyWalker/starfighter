#include <QTimer>
#include <QGraphicsScene>
#include <QList>
#include <stdlib.h>
#include <typeinfo>
#include <QTransform>

#include "item.h"
#include "game.h"
#include "logic.h"
#include "bomb.h"
#include "shot.h"

// Zugriff auf globales Gameobjekt und statische Werte
extern Game *game;
extern const int MECHANIX_POWER;
extern const int ITEM_SPEED;

Item::Item(QGraphicsItem *parent):
    QObject(), QGraphicsPixmapItem(parent), rotationAngle(0)
{
    // 500 Untergrenze des Platzierens
    int randomPosition = rand() % 500;
    setPos(700, randomPosition);

    // Random Wolkenart wählen
    itemArt = rand() % 1;

    // Itemliste wird befüllt (derzeit existiert nur 1 Item)
    itemList.append(QPixmap(":/images/images/items/mechanic.png")); // Flugzeug reparieren

    // Grafik wird gesetzt und die einer Ebene zugewiesen
    setPixmap(itemList[itemArt]);
    this->setZValue(5);

    // Item wird in einer zentralen Liste abgelegt
    game->list_items.append(this);
}

Item::~Item()
{
   game->list_items.removeOne(this);
}


void Item::move()
{

    // Item um die Y-Achse drehen
    changeAngle();

    // Transformation um die vertikale Mitte der Grafik
    QTransform transform;
    transform.translate(pixmap().width()/2, pixmap().height()/2);
    transform.rotate(rotationAngle,Qt::YAxis);
    // Transformation zurücksetzen
    transform.translate(-pixmap().width()/2, -pixmap().height()/2);

    this->setTransform(transform);

    /* Eigentliche Bewegung für das 1. Item
     * jedes Item soll sich später mit einer
     * anderen Geschwindigkeit bewegen */
    if( itemArt == 0) setPos(x()-ITEM_SPEED,y());

    // Kollision Prüfen
    QList<QGraphicsItem* > colliding_items = collidingItems();
    for (int i = 0, n = colliding_items.size(); i < n; i++ )
    {
        // Kollidiert das Item mit dem Spieler?
        if(typeid(*(colliding_items[i])) == typeid(Spieler)){

            // Lebenspunkte erhöhen
            game->health->increase( MECHANIX_POWER );

            // Item wenn aufgesammelt löschen
            scene()->removeItem(this);
            delete this;

            // Schleife beenden
            return;
        }
    }
    // Löschen des Items nach Verlassen des Bildschirmrandes
    if( pos().x() < 0-pixmap().width() ){
        scene()->removeItem(this);
        delete this;
    }
}

void Item::changeAngle()
{
    // Winkel um 2 erhöhen, wenn bei 360, dann
    rotationAngle += 2;
}


