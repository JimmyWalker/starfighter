#ifndef GAME_H
#define GAME_H

#include <QObject>
#include <QGraphicsView>
#include <QWidget>
#include <QGraphicsScene>
#include <QTimer>
#include <QList>
#include <QGraphicsItem>
#include <QGraphicsTextItem>
#include <QMediaPlayer>
#include <QMouseEvent>
#include <QGraphicsRectItem>

#include "spieler.h"
#include "score.h"
#include "health.h"
#include "logic.h"
#include "gamingloop.h"
#include "item.h"
#include "background.h"
#include "bomb.h"
#include "shot.h"
#include "heli.h"

/*!
 * \brief Die Game-Klasse stellt die grafische Oberfläche dar und ist
 *        somit einer der Hauptbestandteile der gesamten Applikation
 *        Sie stellt essentiell wichtige Timer zur Verfügung die einfluss
 *        auf das Spielgeschehen nehmen.
 */
class Game:public QGraphicsView
{

public:
    Game();

    // Container in denen Gameobjekte während der Laufzeit abgelegt werden
    QList<Bomber*> list_bomber;
    QList<Heli*> list_heli;
    QList<Clouds*> list_clouds;
    QList<Item*> list_items;
    QList<Background*> list_backgrounds;
    QList<Bomb*> list_bombs;
    QList<Shot*> list_shots;

    // Hält das Spiel am Laufen
    Gamingloop *gameloop;

    // Spielelogik
    Logic *logic;

    // Spielesczene und ihre grafischen Elemente
    QGraphicsScene *scene;
    Background *bg;
    Background *bg2;
    Spieler *spieler;
    Score *score;
    Health *health;
    QGraphicsRectItem *blackScreen;
    QGraphicsPixmapItem *stageClearScreen;
    QGraphicsPixmapItem *newGame;
    QGraphicsPixmapItem *exitGame;

    // Wert der die Erscheinung eines Übergangbildes bestimmt (bei Levelwechsel)
    double blackScreenOpacityVal;

    // Stattet wichtige Elemente mit Startwerten aus
    void init();
    // Spielt den Sound einer Explosion ab
    void explosion_sound();
    // Leitet den Gameoverbildschirm ein, nachdem der Spieler verloren hat
    void gameOver();
    // Zeigt den Schriftzug "StageCleared" nach dem eine Gegnerwelle bezwungen wurde
    void showStageClear();
    // Entfernt "Stagecleared" Schriftzug
    void removeStageClear();

private:
    // Timer die für wichtie Funktionsaufrufe benötigt werden
    QTimer *loop_timer;
    QTimer *spawn_timer;
    QTimer *cloud_timer;
    QTimer *item_timer;
    QTimer *logic_timer;

    // Empfängt Mousklicks die für den Gameoverbildschirm benötigt werden (weiter ja/nein)
    void mousePressEvent(QMouseEvent *mevent);
    // Löscht alle QTimer
    void delete_Timer();

public slots:
    void setOpacityStageClear();

};

#endif // GAME_H
