#ifndef SPIELER_H
#define SPIELER_H

#include <QGraphicsPixmapItem>
#include <QGraphicsItem>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QObject>


class Spieler: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT

public:
   Spieler(QGraphicsItem * parent=0);
   ~Spieler();

private:
   // Hält Zeitpunkt fest zu dem der Spieler  den grafischen Turbo gezündet hat
   int turbo_timeStamp;

   // regelt die Eingaben mit der Tastatur
   void keyPressEvent(QKeyEvent *event);

public slots:
   // Ein timer kontrolliert ob die Turbografik ausgeblendet werden muss
   void turboOff();

};

#endif // SPIELER_H
